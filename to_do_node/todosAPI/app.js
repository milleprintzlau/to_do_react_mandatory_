const express = require('express');
const fs = require('fs');

const app = express();

app.use(express.json({ type: ['application/json', 'text/plain'] }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

let data = require('../todos.json');

const { todos } = data;
let { autoIncrementedId } = data;

app.get('/todos', (req, res) => res.send({ data: todos }));

app.get('/todos/:id', (req, res) => {
  const integerId = parseInt(req.params.id, 10);
  const todo = todos.find((item) => item.id === integerId);

  return res.send({ data: todo });
});

app.post('/todos', (req, res) => {
  autoIncrementedId += 1;

  const newTodo = { ...req.body, ...{ id: autoIncrementedId } };
  todos.push(newTodo);

  let writeError;
  data = fs.writeFileSync('../todos.json', JSON.stringify({ todos, autoIncrementedId }), (err) => {
    if (err) {
      writeError = err;
    }
  });

  if (writeError) {
    return res.send({ data: writeError });
  }

  return res.send({ data: newTodo });
});

app.patch('/todos/:id', (req, res) => {
  const integerId = parseInt(req.params.id, 10);

  todos.forEach((item, index) => {
    if (item.id === integerId) {
      todos[index].title = req.body.title;
      todos[index].description = req.body.description;
      todos[index].state = req.body.state;
      todos[index].icon = req.body.icon;
    }
  });

  let writeError;
  data = fs.writeFileSync('../todos.json', JSON.stringify({ todos, autoIncrementedId }), (err) => {
    if (err) {
      writeError = err;
    }
  });

  if (writeError) {
    return res.send({ data: writeError });
  }

  return res.send({ data: { updatedTodo: req.body.id } });
});

app.delete('/todos/:id', (req, res) => {
  const integerId = parseInt(req.params.id, 10);

  todos.forEach((item) => {
    if (integerId === item.id) {
      todos.splice(todos.indexOf(item), 1);
    }
  });

  let writeError;
  data = fs.writeFileSync('../todos.json', JSON.stringify({ todos, autoIncrementedId }), (err) => {
    if (err) {
      writeError = err;
    }
  });

  if (writeError) {
    return res.send({ data: writeError });
  }

  return res.send({ data: { deletedItem: req.params.id } });
});

const port = 8082;

app.listen(port, (error) => {
  if (error) {
    console.log(`Error: ${error}`);
  }

  console.log(`Server is running on port: ${port}`);
});
