---
layout: default
title: Documentation
---

This project represent a Todo React/ Node application. It enables its users to do CRUD operations with a todo list.

## The server
The server-side is written in Node. 
To get the server up and running you need to follow these steps:
1. Make sure that npm is installed on your computer, if not install it.
2. Then cd into to_do_node and run npm install.
3. After all the dependencies are done installing cd into todosAPI and run nodemon app.js.
4. By default the server will running on port 8082, it can be changed inside app.js.

#### How does the todos API work?
The API has all the endpoints contained in app.js and they are linked to the todos.json which serves as the project's database.
The API can handle the following actions:
1. Reading all todos GET /todos
2. Getting one todo GET /todos/:id
3. Adding one todo POST /todos
4. Updating one todo PATCH /todos/:id
5. Deleting one todo DELETE /todos/:id

## The client
The server-side is written in React. 
To get the server up and running you need to follow these steps:
1. Make sure that yarn is installed on your computer, if not install it.
2. Then cd into to_do_react and run yarn.
3. After all the dependencies are done installing, yarn start needs to be run to start the project.
4. By default the site will start running on localhost:3000.

To run test the command yarn test needs to be run.

To see more of the available scripts have a look inside package.json.

#### How does the client side work?
The client side code can be found in to_do_react/src with the following folder structure:
1. App - contains App.jsx, the website entry point, and its stylings.
2. components - contains the applications React components grouped by the page they belong to.
3. pages - contains react components that are used as pages for the application.

The application contains two pages: Home and Todos.

Home is just a presentation page whereas Todos has multiple subcomponents responsible for the logic of the CRUD operations.

##### Todos.jsx - what does it do?
Calls the server in order to fetch all the existing todos and splits them into three categories: todo, in progress, done and adds them to the component state.
Holds logic for displaying the AddNewTodo modal and for displaying the EditTodo modal.
The template of this component loads a Todo subcomponent for each todo leaded from the server.


##### Todo.jsx - what does it do?
Takes an item prop which is then used to create a table row with the following columns: Icon, Title, Description, State.
Each to do has an option for edit and for deleting.
Clicking the delete button will trigger a DELETE call to the server, removing the todo from the list.

##### AddNewTodoFrom.jsx - what does it do?
This component is a form which allows the user to add a new todo.
To submit the form at least the title needs to be provided, the icon and description fields are optional and the state will be by default set to todo.
When the form is submitted the server is called and the todo will be added to the list.

##### EditTodoFrom.jsx - what does it do?
This component is a form which takes as a prop the todo id and uses it to call the server and fetch the to do which needs to be updated.
When the from is submitted the server is called again with an UPDATE call.


## General rules
When working with this project all developers are advised to follow these rules:
1. Make sure to have the linter enabled on your IDE so code stylling will be enforced on all the code.
2. Make sure to write new tests if important pieces of code are added.
3. Make sure to run the tests after implementing new code/new tests.
4. Make sure to document all the important changes! (keep it simple and useful). 
