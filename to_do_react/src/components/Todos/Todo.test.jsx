import React from 'react';
import { render, fireEvent, waitForElementToBeRemoved } from '@testing-library/react';
import Todo from './Todo';

test('renders todo correctly', () => {
  const todo = {
    title: 'new todo',
    description: 'description',
    state: 'done',
    icon: 'MdCake',
    id: 1,
  };

  const toDoComponent = render(<Todo item={todo} />);

  expect(toDoComponent.getByText(/Done/i)).toBeInTheDocument();
  expect(toDoComponent.getByText(/new todo/i)).toBeInTheDocument();
  expect(toDoComponent.getByText(/description/i)).toBeInTheDocument();
});

test('deletes todo', () => {
  const todo = {
    title: 'new todo',
    description: 'description',
    state: 'done',
    icon: 'MdCake',
    id: 1,
  };
  window.confirm = () => true;

  const toDoComponent = render(<Todo item={todo} />);

  fireEvent.click(toDoComponent.getByText('x'));

  waitForElementToBeRemoved(() => toDoComponent.getByText('new todo')).then(() => {
    expect(toDoComponent.getByText(/new todo/i)).toBeFalsy();
  });
});
