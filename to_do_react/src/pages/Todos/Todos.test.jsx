import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Todos from './Todos';

test('renders table head correctly', () => {
	const toDosComponent = render(<Todos />);

  expect(toDosComponent.getByText(/Name/i)).toBeInTheDocument();
  expect(toDosComponent.getByText(/Description/i)).toBeInTheDocument();
  expect(toDosComponent.getByText(/State/i)).toBeInTheDocument();
  expect(toDosComponent.getByText(/New todo/i)).toBeInTheDocument();
});
